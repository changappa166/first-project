//
//  ViewController.swift
//  Example
//
//  Created by Changappa K S on 23/09/20.
//  Copyright © 2020 Changappa K S. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func gotoSecondViewPressed(_ sender: UIBarButtonItem) {
       // performSegue(withIdentifier: "goToSecond", sender: self)
        
        let mapViewControllerObj = self.storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as? SecondViewController
        self.navigationController?.pushViewController(mapViewControllerObj!, animated: true)
        
//        self.storyboard?.instantiateViewController(identifier: "AddOrderTableViewController") as? AddOrderTableViewController else { fatalError("Unable to instantiateVC") }
//        orderVC.delegate = self
//        self.navigationController?.pushViewController(orderVC, animated: true)
    }
}

